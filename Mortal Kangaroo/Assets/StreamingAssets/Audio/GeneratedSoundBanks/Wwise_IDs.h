/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID DARGOLPE = 1175720125U;
        static const AkUniqueID DARPASO = 648224961U;
        static const AkUniqueID DISPARARLADRILLOS = 2804550137U;
        static const AkUniqueID MXFUERAMENUS = 3437296247U;
        static const AkUniqueID MXMENUS = 760183614U;
        static const AkUniqueID P1 = 1635194252U;
        static const AkUniqueID P2 = 1635194255U;
        static const AkUniqueID P3 = 1635194254U;
        static const AkUniqueID P4 = 1635194249U;
        static const AkUniqueID PARARMUSICA = 2160515625U;
        static const AkUniqueID PLAYMX = 935210936U;
        static const AkUniqueID RECIBIRGOLPE = 98641072U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace MUSICA
        {
            static const AkUniqueID GROUP = 1730564739U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID P1 = 1635194252U;
                static const AkUniqueID P2 = 1635194255U;
                static const AkUniqueID P3 = 1635194254U;
                static const AkUniqueID P4 = 1635194249U;
            } // namespace STATE
        } // namespace MUSICA

    } // namespace STATES

    namespace SWITCHES
    {
        namespace MUSICA
        {
            static const AkUniqueID GROUP = 1730564739U;

            namespace SWITCH
            {
                static const AkUniqueID P1 = 1635194252U;
                static const AkUniqueID P2 = 1635194255U;
                static const AkUniqueID P3 = 1635194254U;
                static const AkUniqueID P4 = 1635194249U;
            } // namespace SWITCH
        } // namespace MUSICA

    } // namespace SWITCHES

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN = 3161908922U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
