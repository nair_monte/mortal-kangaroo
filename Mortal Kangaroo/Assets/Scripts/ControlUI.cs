using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlUI : MonoBehaviour
{
    public Image barraDeVidaP1;
    public Image barraDeVidaP2;
    public float vidaMaxima = 100f;
  

   
    private float anchoMaximo = 757.9156f;
    private float anchoMinimo = 0f;

    public TMPro.TMP_Text txtTimerRonda;
    public TMPro.TMP_Text txtRondas;
    public TMPro.TMP_Text txtInicioRonda;


    public Image municion1P1;
    public Image municion2P1;
    public Image municion3P1;


    public Image municion1P2;
    public Image municion2P2;
    public Image municion3P2;

    private GameObject p1;
    private GameObject p2;

    private GameObject MenuPausa;

    private GameObject MenuGanador;
    public TMPro.TMP_Text txtGanador;
    private bool enMenuGanador = false;

    private GameObject MenuControles;
    private bool enMenuControles=false;

    private GameObject MenuColoresGeneral;
    private GameObject MenuColoresP1;
    private GameObject MenuColoresP2;
    private void Start()
    {
        p1 = GameObject.FindGameObjectWithTag("Player1");
        p2 = GameObject.FindGameObjectWithTag("Player2");
        MenuPausa = GameObject.FindGameObjectWithTag("MenuPausa");
        MenuGanador = GameObject.FindGameObjectWithTag("MenuGanador");
        MenuControles = GameObject.FindGameObjectWithTag("MenuCombos");
        MenuColoresGeneral = GameObject.FindGameObjectWithTag("MenuColoresGeneral");
        MenuColoresP1 = GameObject.FindGameObjectWithTag("MenuColoresP1");
        MenuColoresP2 = GameObject.FindGameObjectWithTag("MenuColoresP2");
        municion1P1.enabled = true;
        municion1P2.enabled = true;
        municion2P1.enabled = true;
        municion2P2.enabled = true;
        municion3P1.enabled = true;
        municion3P2.enabled = true;

        MenuControles.SetActive(false);
        desactivarMenu(MenuColoresGeneral);
        desactivarMenu(MenuColoresP1);
        desactivarMenu(MenuColoresP2);
    }
    private void Update()
    {
        contInicioRonda();

        actualizarMuniciones(p1, municion1P1,municion2P1,municion3P1);
        actualizarMuniciones(p2,municion1P2,municion2P2,municion3P2);

        ActualizarBarraDeVida(barraDeVidaP2, GameManager.vidaActualP2);
        ActualizarBarraDeVida(barraDeVidaP1, GameManager.vidaActualP1);

        txtRondas.text = GameManager.rondasGanadasP2 + "   -   " + GameManager.rondasGanadasP1;

        if (GameManager.TimerDeRonda >= 100)
        {
            txtTimerRonda.text = GameManager.TimerDeRonda.ToString("f0");

        }
        else if(GameManager.TimerDeRonda >= 10    &&   GameManager.TimerDeRonda<= 99)
        {
            txtTimerRonda.text = "0"+GameManager.TimerDeRonda.ToString("f0");

        }
        else if(GameManager.TimerDeRonda <= 9)
        {

            txtTimerRonda.text = "00" + GameManager.TimerDeRonda.ToString("f0");
        }

      

        //texto de ganador final
        if (GameManager.rondasGanadasP1 >= 3 || GameManager.rondasGanadasP2 >= 3)
        {
            MenuGanador.SetActive(true);
            txtGanador.enabled = true;
            txtGanador.text = GameManager.chequearGanador();
            enMenuGanador = true;
        }
        else
        {
            MenuGanador.SetActive(false);
            txtGanador.enabled = false;

            enMenuGanador = false;
        }

       if(GameManager.enPausa&& !MenuGanador.active)
        {
            MenuPausa.SetActive(true);
           
        }
       else
        {
            desactivarMenu();
           
        }

       if(GameManager.enPausa || enMenuControles || enMenuGanador)
        {
            Cursor.visible = true;
        }
       else
        {
            Cursor.visible = false;
        }

      
    }

   public void actualizarMuniciones(GameObject player, Image img1, Image img2, Image img3)
    {
        if (player.GetComponent<ComportamientoPersonaje1>().lstLadrillos.Count >= 3)
        {
            img1.enabled = true;
            img2.enabled = true;
            img3.enabled = true;
        }
        else if (player.GetComponent<ComportamientoPersonaje1>().lstLadrillos.Count == 2)
        {
            img1.enabled = true;
            img2.enabled = true;
            img3.enabled = false;
        }
        else if (player.GetComponent<ComportamientoPersonaje1>().lstLadrillos.Count == 1)
        {
            img1.enabled = true;
            img2.enabled = false;
            img3.enabled = false;
        }

        else
        {
            img1.enabled = false;
            img2.enabled = false;
            img3.enabled = false;
        }
            
        

        
    
    }
    private void  contInicioRonda()
    {
        if (GameManager.TimerDeRonda >= 119 && GameManager.TimerDeRonda < 120)
        {
            txtInicioRonda.color = Color.white;
            txtInicioRonda.text = "3";
            txtInicioRonda.enabled = true;
        }

        else if (GameManager.TimerDeRonda >= 118 && GameManager.TimerDeRonda < 119)
            txtInicioRonda.text = "2";
        else if (GameManager.TimerDeRonda >= 117 && GameManager.TimerDeRonda < 118)
            txtInicioRonda.text = "1";
        else if (GameManager.TimerDeRonda >= 116 && GameManager.TimerDeRonda < 117)
        {
            txtInicioRonda.color = Color.red;
            txtInicioRonda.text = "FIGHT";
        }
        else
            txtInicioRonda.enabled = false;
    }
    public void ActualizarBarraDeVida(Image barraDeVida, int vidaActual)
    {
        //sacar proporcion de vida
        float proporcionVida = vidaActual / vidaMaxima;

        //calcular ancho objetivo en base a la proporcion de vida
        float anchoDeseado = Mathf.Lerp(anchoMinimo, anchoMaximo, proporcionVida);

        //cambiar el width de la barra
        RectTransform barraTransform = barraDeVida.rectTransform;
        barraTransform.sizeDelta = new Vector2(anchoDeseado, barraTransform.sizeDelta.y);
    }
 
    public void activarMenuControles()
    {
        MenuControles.SetActive(true);
        enMenuControles = true ;
    }

    public void desactivarMenuControles()
    {
        MenuControles.SetActive(false);
        enMenuControles = false;
    }

    public void desactivarMenu(GameObject menu)
    {
        menu.SetActive(false);

        if (menu == MenuControles)
            enMenuControles = false;

    }

    public void activarMenu(GameObject menu)
    {
        menu.SetActive(true);

        if (menu == MenuControles)
            enMenuControles = true;

    }

    public void desactivarMenu()
    {
        MenuPausa.SetActive(false);
        MenuColoresP1.SetActive(false);
        MenuColoresP2.SetActive(false);
        MenuColoresGeneral.SetActive(false);
        MenuControles.SetActive(false);
        
    }

}
