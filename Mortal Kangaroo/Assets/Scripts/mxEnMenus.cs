using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mxEnMenus : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(GameManager.enPausa)
            AkSoundEngine.PostEvent("mxMenus", this.gameObject);
        if(!GameManager.enPausa)
            AkSoundEngine.PostEvent("mxFueraMenus", this.gameObject);
    }
}
