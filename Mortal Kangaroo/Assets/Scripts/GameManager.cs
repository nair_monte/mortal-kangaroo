using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static float TimerDeRonda = 120f;

    public static int vidaActualP1 = 100;
    public static int vidaActualP2 = 100;
    public static Transform posInicialP1;
    public static Transform posInicialP2;

    public static int rondasGanadasP1 = 0;
    public static int rondasGanadasP2 = 0;

    public GameObject mapa1;
    public GameObject mapa2;
    public GameObject mapa3;
    public int numMapa = 0;

    static GameObject[] lavas;
    internal static bool playerEnLava = false;

    internal protected static bool enPausa = false;
    public float timeTeclaPausa1 = 0f;
    public float timeTeclaPausa2 = 0f;

    private static GameObject p1;
    private static GameObject p2;

    private GameObject ControlMX;

    private GameObject recolectableActual;
    internal static float timerRecolectable = 0;
    public static object SceneManagment { get; private set; }

    void Start()
    {
        timerRecolectable = 0;

        ControlMX = GameObject.Find("ControlMX");
        lavas = GameObject.FindGameObjectsWithTag("Lava");

        posInicialP1 = GameObject.Find("posInicialP1").transform;
        posInicialP2 = GameObject.Find("posInicialP2").transform;

        p1 = GameObject.FindGameObjectWithTag("Player1");
        p2 = GameObject.FindGameObjectWithTag("Player2");
        resetRonda();


    }

    // Update is called once per frame
    void Update()
    {
        respawnRecolectables();


        if (Input.GetKeyDown(KeyCode.Escape) && Time.time - timeTeclaPausa1 > 0.5f)
        {
            enPausa = !enPausa;
            timeTeclaPausa1 = Time.time;



        }
        if (Input.GetKeyDown(KeyCode.F4))
        {
            resetRonda();
        }
        if (Input.GetKeyDown(KeyCode.F5))
        {
            resetGame();
        }

        TimerDeRonda -= Time.deltaTime;

        vidaActualP1 = p1.GetComponent<ComportamientoPersonaje1>().vidaPlayer;
        vidaActualP2 = p2.GetComponent<ComportamientoPersonaje1>().vidaPlayer;



        if (vidaActualP1 <= 0 && vidaActualP2 > 0)
        {
            rondasGanadasP2++;
            resetRonda();
        }

        else if (vidaActualP2 <= 0 && vidaActualP1 > 0)
        {
            rondasGanadasP1++;
            resetRonda();
        }
        else if (vidaActualP2 <= 0 && vidaActualP1 <= 0)
        {
            rondasGanadasP1++;
            rondasGanadasP2++;
            resetRonda();
        }

        if (TimerDeRonda <= 0)
        {
            rondasGanadasP1++;
            rondasGanadasP2++;
            resetRonda();

        }

        if (TimerDeRonda >= 117)
            resetPosPlayers();
    }

    public void actualizarTimer()
    {
        TimerDeRonda -= Time.deltaTime;

    }

    public static void resetTimer()
    {
        TimerDeRonda = 120f;

    }

    public void resetRonda()
    {
        p1.GetComponent<ComportamientoPersonaje1>().vidaPlayer = 100;
        p2.GetComponent<ComportamientoPersonaje1>().vidaPlayer = 100;


        vidaActualP1 = p1.GetComponent<ComportamientoPersonaje1>().vidaPlayer;
        vidaActualP2 = p2.GetComponent<ComportamientoPersonaje1>().vidaPlayer;
        p1.transform.position = posInicialP1.position;
        p2.transform.position = posInicialP2.position;
        resetTimer();

        activarMapas();
        resetPosLavas();
        //resetEspadasObjetos();
        resetPosPlayers();

        cambiarMapa(Random.Range(1, 3 + 1));

        ControlMX.GetComponent<mxPorRondas>().activarMusica();
    }

    public static string chequearGanador()
    {
        string ganador = "";
        if (rondasGanadasP1 >= 3 && rondasGanadasP2 < 3)
        {

            ganador = "Gano el player 1";
        }
        else if (rondasGanadasP2 >= 3 && rondasGanadasP1 < 3)
        {
            ganador = "Gano el player 2";

        }
        else if (rondasGanadasP2 >= 3 && rondasGanadasP1 >= 3)
        {
            ganador = "Empate";
        }

        return ganador;
    }

    public void spawnRecolectables(ref GameObject mapa)
    {
        int rnd = Random.Range(1, 3 + 1);


        if (rnd == 1)
        {
            mapa.GetComponent<Mapa>().Espada.gameObject.SetActive(false);
            mapa.GetComponent<Mapa>().Escudo.gameObject.SetActive(true);
            mapa.GetComponent<Mapa>().Vida.gameObject.SetActive(false);
            recolectableActual = mapa.GetComponent<Mapa>().Escudo.gameObject;
        }
        else if(rnd==2)
        {
            mapa.GetComponent<Mapa>().Espada.gameObject.SetActive(true);
            mapa.GetComponent<Mapa>().Escudo.gameObject.SetActive(false);
            mapa.GetComponent<Mapa>().Vida.gameObject.SetActive(false);
            recolectableActual = mapa.GetComponent<Mapa>().Espada.gameObject;
        }

        else
        {
            mapa.GetComponent<Mapa>().Espada.gameObject.SetActive(false);
            mapa.GetComponent<Mapa>().Escudo.gameObject.SetActive(false);
            mapa.GetComponent<Mapa>().Vida.gameObject.SetActive(true);
            recolectableActual = mapa.GetComponent<Mapa>().Vida.gameObject;
        }

    }

    public void respawnRecolectables()
    {

        if (Time.time - timerRecolectable >= 20)
            recolectableActual.SetActive(true);
    }
    public void resetGame()
    {
        rondasGanadasP1 = 0;
        rondasGanadasP2 = 0;
        resetRonda();

    }
    public void activarMapas()
    {
        mapa1.SetActive(true);
        mapa2.SetActive(true);
        mapa3.SetActive(true);

    }
    public void cambiarMapa(int numMapa)
    {
        if (numMapa == 1)
        {
            this.numMapa = 1;
            mapa1.SetActive(true);
            mapa2.SetActive(false);
            mapa3.SetActive(false);
            spawnRecolectables(ref mapa1);
        }
        else if (numMapa == 2)
        {
            this.numMapa = 2;
            mapa1.SetActive(false);
            mapa2.SetActive(true);
            mapa3.SetActive(false);
            spawnRecolectables(ref mapa2);
        }
        else
        {
            this.numMapa = 3;
            mapa1.SetActive(false);
            mapa2.SetActive(false);
            mapa3.SetActive(true);
            spawnRecolectables(ref mapa3);

        }


    }

    public static void resetPosPlayers()
    {
        p1.transform.position = posInicialP1.position;
        p2.transform.position = posInicialP2.position;

    }
    public static void resetPosLavas()
    {
        if (GameManager.vidaActualP1 <= 0 || GameManager.vidaActualP2 <= 0)
        {



            foreach (GameObject lava in lavas)
            {
                lava.transform.position = lava.gameObject.GetComponent<Lava>().posInicial;
            }
        }
    }

    //public static void resetEspadasObjetos()
    //{
    //    GameObject[] espadasObjetos = GameObject.FindGameObjectsWithTag("EspadaObjeto");

    //    foreach (GameObject espada in espadasObjetos)
    //    {

    //        espada.SetActive(true);
    //    }


    //}

    public static void cambiarModoPausa()
    {
        enPausa = !enPausa;
    }
    public static void volverAlMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
