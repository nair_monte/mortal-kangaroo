using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mxPorRondas : MonoBehaviour
{
   
    void Start()
    {
        
       
    }

    // Update is called once per frame
    void Update()
    {
        activarMusica();

    }

    public void activarMusica()
    {
        if (GameManager.rondasGanadasP1 == 0 && GameManager.rondasGanadasP2 == 0)
            AkSoundEngine.PostEvent("p1", this.gameObject);

        else if ((GameManager.rondasGanadasP1 == 1 && GameManager.rondasGanadasP2 <= 1) ||
           (GameManager.rondasGanadasP2 == 1 && GameManager.rondasGanadasP1 < 2))
            AkSoundEngine.PostEvent("p2", this.gameObject);

       else if ((GameManager.rondasGanadasP1==2 && GameManager.rondasGanadasP2 <2)||
            (GameManager.rondasGanadasP2==2&& GameManager.rondasGanadasP1<2))
             AkSoundEngine.PostEvent("p3", this.gameObject);

        else
            AkSoundEngine.PostEvent("p4", this.gameObject);



    }
}
