using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sfxPasos : MonoBehaviour
{
    private ComportamientoPersonaje1 compPJ;
    private float CDPasos;
    void Start()
    {
        compPJ = GetComponent<ComportamientoPersonaje1>();  
    }

    // Update is called once per frame
    void Update()
    {
       
        if (compPJ.enPiso &&    !compPJ.agachado    && compPJ.mov != Vector3.zero && (Time.time-CDPasos) > 0.3f)
        {
            AkSoundEngine.PostEvent("darPaso", this.gameObject);
            CDPasos = Time.time;
        }
    }
}
