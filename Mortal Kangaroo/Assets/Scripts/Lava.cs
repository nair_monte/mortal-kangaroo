using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lava : MonoBehaviour
{
    public int movimientoNum = 0;
    public float velMovs = 0.3f;
    public Vector3 posInicial;

    
    void Start()
    {
        posInicial  = new Vector3(transform.position.x, transform.position.y, transform.position.z); 
    }

    // Update is called once per frame
    void Update()
    {
       
       
        if(GameManager.TimerDeRonda<=40 && movimientoNum != 0)
        {
            Moverse(movimientoNum);

        }
       if(GameManager.TimerDeRonda >=40)
        {

            transform.position = posInicial;
        }
    }

    public void Moverse(int movimiento)
    {
        if (movimiento == 1)
        {
            Bajar();
        }
        else if (movimiento == 2)
        {
            Subir();
        }
        else if (movimiento == 3)
        {
            Derecha();
        }
        else
        {
            Izquierda();
        }

    }
    private void Subir()
    {
        transform.position += Vector3.up *velMovs *Time.deltaTime;

    }
    private void Bajar()
    {
        transform.position -= Vector3.up * velMovs * Time.deltaTime;

    }
    private void Derecha()
    {
        transform.position += Vector3.right * velMovs * Time.deltaTime;

    }
    private void Izquierda()
    {
        transform.position -= Vector3.right * velMovs * Time.deltaTime;

    }

  
}
