using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Escudo : MonoBehaviour
{
    private GameObject jugadorActual;
    public bool esUnRecolectable = false;
    void Start()
    {
        if (!esUnRecolectable)
        {
            if (transform.GetComponentInParent<Player1>())
                jugadorActual = GameObject.FindGameObjectWithTag("Player1");

            else if (transform.GetComponentInParent<Player2>())
                jugadorActual = GameObject.FindGameObjectWithTag("Player2");
        }


    }

    private void OnTriggerEnter(Collider other)
    {
        if(esUnRecolectable )
        {
            if (other.gameObject.GetComponent<Player>())                                   
                this.gameObject.SetActive(false);
            
        }
    }
}
