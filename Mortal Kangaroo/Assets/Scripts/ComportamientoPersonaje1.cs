using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComportamientoPersonaje1 : MonoBehaviour
{

    public float velMovimiento = 5f;
    public float fuerzaSalto = 2f;
    public float gravedad = -9.8f;

    private CharacterController characterController;
    private Vector3 vel;
    public bool enPiso;
    public bool enLava;

    public LayerMask layerPiso;
    public LayerMask layerLava;

    public int numPlayer = 1;

    private GameObject cuerpoPlayer;

    private Animator animator;
    public bool ataqueBasico = false;
    public bool ataqueBajo = false;
    public bool ataqueDesdeArriba = false;
    public bool haciendoAlgo = false;

    public bool bloqueandoGolpes = false;
    public bool bloqueandoGolpesBajos = false;
    public bool agachado = false;
    public bool saltando = false;

    public int vidaPlayer = 100;

    private ComportamientoPersonaje1 p1;
    private ComportamientoPersonaje1 p2;

    private bool recibirDa�o = false;

    public int ejecutandoAtaqueEspecialIzq = 0;
    public int ejecutandoAtaqueEspecialDer = 0;
    public bool ataqueBasicoDistancia = false;


    public int ejecutandoAtaqueEspecialBajoDer = 0;
    public int ejecutandoAtaqueEspecialBajoIzq = 0;
    public bool ataqueBajoDistancia = false;

    public int ejecutandoAtaqueSupermanIzq = 0;
    public int ejecutandoAtaqueSupermanDer = 0;
    public bool ataqueSuperman = false;

    //especial basico der distancia
    public KeyCode ultTeclaCombo1 = KeyCode.None;
    public float timeCombo1 = 0f;

    //especial basico izq distancia
    public KeyCode ultTeclaCombo2 = KeyCode.None;
    public float timeCombo2 = 0f;

    //especial bajo der distancia
    public KeyCode ultTeclaCombo3 = KeyCode.None;
    public float timeCombo3 = 0f;

    //especial bajo izq distancia
    public KeyCode ultTeclaCombo4 = KeyCode.None;
    public float timeCombo4 = 0f;

    //especial basico izq distancia (melee)
    public KeyCode ultTeclaCombo5 = KeyCode.None;
    public float timeCombo5 = 0f;

    public float timeUltTeclaBajo = 0f;
    public GameObject ladrilloBasicoPrefab;
    public GameObject ladrilloBajoPrefab;

    public Player playerActual;
    private Vector3 checkBoxTama�o;



    internal protected List<GameObject> lstLadrillos;
    public float CDRegenLadrillo = 3;


    public bool contraAtaqueHabilitado = false;
    public float timeParry = 0f;
    public bool contraAtacando = false;
    private Espada espadaDelPlayer;
    private Escudo escudoDelPlayer;
    public bool conEscudo = false;

    //movimiento

    public float movX = 0;



    public Vector3 mov;
    void Start()
    {
        espadaDelPlayer = GetComponentInChildren<Espada>();
        escudoDelPlayer = GetComponentInChildren<Escudo>();
        desactivarEspada();
        desactivarEscudo();

        lstLadrillos = new List<GameObject> { new GameObject(), new GameObject(), new GameObject() };

        characterController = GetComponent<CharacterController>();

        //primer hijo
        cuerpoPlayer = transform.GetChild(0).gameObject;

        animator = transform.GetComponentInChildren<Animator>();

        checkBoxTama�o = new Vector3(0.3f, 1f, 0.3f);

        p1 = GameObject.FindGameObjectWithTag("Player1").GetComponent<ComportamientoPersonaje1>();
        p2 = GameObject.FindGameObjectWithTag("Player2").GetComponent<ComportamientoPersonaje1>();


        //player 2
        if (this.gameObject.GetComponent<Player2>())
        {
            playerActual = new Player2();
            playerActual = this.gameObject.GetComponent<Player2>();
        }
        //player 1
        else if (this.gameObject.GetComponent<Player1>())
        {
            playerActual = new Player1();
            playerActual = this.gameObject.GetComponent<Player1>();

        }

    }

    // Update is called once per frame
    void Update()
    {
        movX = 0;

        //player 2
        if (this.gameObject.GetComponent<Player2>())
        {

            playerActual = this.gameObject.GetComponent<Player2>();
        }
        //player 1
        else if (this.gameObject.GetComponent<Player1>())
        {

            playerActual = this.gameObject.GetComponent<Player1>();

        }






        enPiso = Physics.CheckBox(transform.position, checkBoxTama�o, Quaternion.identity, layerPiso);

        enLava = Physics.CheckBox(transform.position, checkBoxTama�o, Quaternion.identity, layerLava);

        if (enLava)
        {


        }

        //animacion saltando
        if (!enPiso && !haciendoAlgo)
        {
            saltando = true;
            animator.SetBool("saltando", true);


        }
        else
        {
            saltando = false;
            animator.SetBool("saltando", false);
        }



        if (enPiso && vel.y < 0)
        {
            vel.y = 0f;
        }



        if (!haciendoAlgo && !ataqueBasicoDistancia && !ataqueBajoDistancia && !ataqueSuperman && GameManager.TimerDeRonda <= 117f)

        {

            //d

            if (Input.GetKey(playerActual.derInput))
            {
                movX = 1;
            }

            //a

            if (Input.GetKey(playerActual.izqInput))
            {
                movX = -1;
            }

            mov = transform.right * movX;

            characterController.Move(mov * velMovimiento * Time.deltaTime);



            if ((mov.x >= 0.1f || mov.z >= 0.1f || mov.x <= -0.1f || mov.z <= -0.1f) && !saltando)
            {
                animator.SetBool("moviendose", true);
            }
            else
            {
                animator.SetBool("moviendose", false);
            }

        }
        //en el combo superman se mueve mas rapido
        else if (ataqueSuperman)
        {

            //d
            if (Input.GetKey(playerActual.derInput))
            {
                movX = 1;
            }
            //a
            if (Input.GetKey(playerActual.izqInput))
            {
                movX = -1;
            }

            mov = transform.right * movX;





            characterController.Move(mov * (velMovimiento * 2) * Time.deltaTime);

        }






        //rotacion del cuerpo
        if (movX > 0)
        {
            cuerpoPlayer.transform.rotation = new Quaternion(0, -180, 0, 0);
        }
        else if (movX < 0)
        {
            cuerpoPlayer.transform.rotation = new Quaternion(0, 0, 0, 0);
        }

        if (contraAtaqueHabilitado)
        {
            //contraAtaque basico
            ContraAtaque(playerActual);

        }
        else
        {
            //ataque basico
            AtaqueBasico(playerActual);
        }

        if (!contraAtacando)
        {
            animator.SetBool("contraAtaque", contraAtacando);
        }


        if (GameManager.TimerDeRonda <= 117)
        {

            //golpe superman izquierda
            ComboSupermanIzq(playerActual);

            //golpe desde arriba
            GolpeDesdeArriba(playerActual);


            //bloqueo golpes normales
            BloqueoGolpesNormales(playerActual);

            //salto
            Salto(playerActual);

            //agacharse
            Agacharse(playerActual);

            //ataque bajo
            AtaqueBajo(playerActual);

            //bloqueo bajo
            BloqueoBajo(playerActual);

            //ataques especiales izquierda  l j l j o
            AtaqueEspecialIzq(playerActual);

            //ataques especiales derecha j l j l u
            AtaqueEspecialDer(playerActual);

            //ataque especial bajo derecha k l u
            AtaqueEspecialBajoDer(playerActual);

            //ataque especial bajo izq k u j u
            AtaqueEspecialBajoIzq(playerActual);
        }


    }

    public void ComboSupermanIzq(Player pJugador)
    {
        //q e a q/ q e d q

        //q
        if (Input.GetKeyDown(pJugador.ataqueInput) && ejecutandoAtaqueSupermanIzq == 0)
        {
            ejecutandoAtaqueSupermanIzq = 1;

            ultTeclaCombo5 = pJugador.ataqueInput;
            timeCombo5 = Time.time;

        }
        // e
        else if (Input.GetKeyDown(pJugador.bloqueoInput) && ejecutandoAtaqueSupermanIzq == 1)
        {
            ejecutandoAtaqueSupermanIzq = 2;
            ultTeclaCombo5 = pJugador.bloqueoInput;
            timeCombo5 = Time.time;

        }
        //a
        else if (Input.GetKeyDown(pJugador.izqInput) && ejecutandoAtaqueSupermanIzq == 2)
        {
            ejecutandoAtaqueSupermanIzq = 3;
            ultTeclaCombo5 = pJugador.izqInput;
            timeCombo5 = Time.time;

        }
        //q
        if (Input.GetKeyDown(pJugador.ataqueInput) && ejecutandoAtaqueSupermanIzq == 3)
        {
            ejecutandoAtaqueSupermanIzq = 0;

            ataqueSuperman = true;
            haciendoAlgo = true;

            animator.SetBool("ataqueSuperman", ataqueSuperman);
        }
        else if (Input.GetKeyUp(pJugador.ataqueInput))
        {
            Invoke("resetAtaqueSuperman", 1.5f);
            Invoke("resetHaciendoAlgo", 1f);
        }

        if ((Time.time - timeCombo5) >= 0.5f)
        {
            ejecutandoAtaqueSupermanIzq = 0;
        }

    }
    public void AtaqueEspecialBajoIzq(Player pJugador)
    {
        //s
        if (Input.GetKeyDown(pJugador.abajoInput) && ejecutandoAtaqueEspecialBajoIzq == 0)
        {
            ejecutandoAtaqueEspecialBajoIzq = 1;

            ultTeclaCombo4 = pJugador.abajoInput;
            timeCombo4 = Time.time;
        }

        //q
        else if (Input.GetKeyDown(pJugador.ataqueInput) && ejecutandoAtaqueEspecialBajoIzq == 1 && ultTeclaCombo4 == pJugador.abajoInput)
        {

            ejecutandoAtaqueEspecialBajoIzq = 2;

            ultTeclaCombo4 = pJugador.ataqueInput;
            timeCombo4 = Time.time;
        }
        //a
        else if (Input.GetKeyDown(pJugador.izqInput) && ejecutandoAtaqueEspecialBajoIzq == 2 && ultTeclaCombo4 == pJugador.ataqueInput)
        {

            ejecutandoAtaqueEspecialBajoIzq = 3;

            ultTeclaCombo4 = pJugador.izqInput;
            timeCombo4 = Time.time;



            ejecutandoAtaqueEspecialBajoDer = 0;
        }
        //q
        if (Input.GetKeyDown(pJugador.ataqueInput) && ejecutandoAtaqueEspecialBajoIzq == 3 && ultTeclaCombo4 == pJugador.izqInput)
        {


            ejecutandoAtaqueEspecialBajoIzq = 0;

            ataqueBajoDistancia = true;
            animator.SetBool("ataqueBajoDistancia", ataqueBajoDistancia);
            haciendoAlgo = true;
            if (lstLadrillos.Count > 0)
            {
                GameObject ladrillo = Instantiate(ladrilloBajoPrefab, transform.position - transform.right - transform.up * 0.1f, Quaternion.identity);
                ladrillo.GetComponent<Rigidbody>().AddForce(-Vector3.right * 10f, ForceMode.VelocityChange);
                lstLadrillos.RemoveAt(0);
                Invoke("agregarMunicion", CDRegenLadrillo);
            }

        }
        else if (Input.GetKeyUp(pJugador.ataqueInput))
        {
            Invoke("resetAtaqueBajoDistancia", 0.29f);
            Invoke("resetHaciendoAlgo", 0.29f);
        }

        if ((Time.time - timeCombo4) >= 0.5f)
        {
            ejecutandoAtaqueEspecialBajoIzq = 0;
        }
    }
    public void AtaqueEspecialBajoDer(Player pJugador)
    {
        //s
        if (Input.GetKeyDown(pJugador.abajoInput) && ejecutandoAtaqueEspecialBajoDer == 0)
        {
            ejecutandoAtaqueEspecialBajoDer = 1;

            ultTeclaCombo3 = pJugador.abajoInput;
            timeCombo3 = Time.time;
        }


        //q
        else if (Input.GetKeyDown(pJugador.ataqueInput) && ejecutandoAtaqueEspecialBajoDer == 1 && ultTeclaCombo3 == pJugador.abajoInput)
        {

            ejecutandoAtaqueEspecialBajoDer = 2;

            ultTeclaCombo3 = pJugador.ataqueInput;
            timeCombo3 = Time.time;




        }
        //d
        else if (Input.GetKeyDown(pJugador.derInput) && ejecutandoAtaqueEspecialBajoDer == 2 && ultTeclaCombo3 == pJugador.ataqueInput)
        {

            ejecutandoAtaqueEspecialBajoDer = 3;

            ultTeclaCombo3 = pJugador.derInput;
            timeCombo3 = Time.time;



            ejecutandoAtaqueEspecialBajoIzq = 0;
        }
        //q
        if (Input.GetKeyDown(pJugador.ataqueInput) && ejecutandoAtaqueEspecialBajoDer == 3 && ultTeclaCombo3 == pJugador.derInput)
        {


            ejecutandoAtaqueEspecialBajoDer = 0;

            ataqueBajoDistancia = true;
            animator.SetBool("ataqueBajoDistancia", ataqueBajoDistancia);
            haciendoAlgo = true;

            if (lstLadrillos.Count > 0)
            {
                GameObject ladrillo = Instantiate(ladrilloBajoPrefab, transform.position + transform.right - transform.up * 0.1f, Quaternion.identity);
                ladrillo.GetComponent<Rigidbody>().AddForce(Vector3.right * 10f, ForceMode.VelocityChange);
                lstLadrillos.RemoveAt(0);
                Invoke("agregarMunicion", CDRegenLadrillo);
            }
        }
        else if (Input.GetKeyUp(pJugador.ataqueInput))
        {
            Invoke("resetAtaqueBajoDistancia", 0.29f);
            Invoke("resetHaciendoAlgo", 0.29f);
        }

        if ((Time.time - timeCombo3) >= 0.5f)
        {
            ejecutandoAtaqueEspecialBajoDer = 0;
        }



    }
    public void AtaqueEspecialDer(Player pJugador)
    {
        //a
        if (Input.GetKeyDown(pJugador.izqInput) && ejecutandoAtaqueEspecialDer == 0)
        {
            ejecutandoAtaqueEspecialDer = 1;

            ultTeclaCombo2 = pJugador.izqInput;
            timeCombo2 = Time.time;
        }


        //d
        if (Input.GetKeyDown(pJugador.derInput) && ejecutandoAtaqueEspecialDer == 1 && ultTeclaCombo2 == pJugador.izqInput)
        {

            ejecutandoAtaqueEspecialDer = 2;

            ultTeclaCombo2 = pJugador.derInput;
            timeCombo2 = Time.time;

            ejecutandoAtaqueEspecialIzq = 0;
            ejecutandoAtaqueEspecialBajoDer = 0;
            ejecutandoAtaqueEspecialBajoIzq = 0;
        }


        //a
        if (Input.GetKeyDown(pJugador.izqInput) && ejecutandoAtaqueEspecialDer == 2 && ultTeclaCombo2 == pJugador.derInput)
        {
            ejecutandoAtaqueEspecialDer = 3;

            ultTeclaCombo2 = pJugador.izqInput;
            timeCombo2 = Time.time;

            ejecutandoAtaqueEspecialIzq = 0;
            ejecutandoAtaqueEspecialBajoDer = 0;
            ejecutandoAtaqueEspecialBajoIzq = 0;
        }
        //d
        if (Input.GetKeyDown(pJugador.derInput) && ejecutandoAtaqueEspecialDer == 3 && ultTeclaCombo2 == pJugador.izqInput)
        {
            ejecutandoAtaqueEspecialDer = 4;

            ultTeclaCombo2 = pJugador.derInput;
            timeCombo2 = Time.time;

            ejecutandoAtaqueEspecialIzq = 0;
            ejecutandoAtaqueEspecialBajoDer = 0;
            ejecutandoAtaqueEspecialBajoIzq = 0;
        }
        //q
        if (Input.GetKeyDown(pJugador.ataqueInput) && ejecutandoAtaqueEspecialDer == 4 && ultTeclaCombo2 == pJugador.derInput)
        {


            ejecutandoAtaqueEspecialDer = 0;
            ataqueBasicoDistancia = true;
            animator.SetBool("ataqueBasicoDistancia", ataqueBasicoDistancia);
            haciendoAlgo = true;

            if (lstLadrillos.Count > 0)
            {

                GameObject ladrillo = Instantiate(ladrilloBasicoPrefab, transform.position + transform.right + transform.up * 0.5f, Quaternion.identity);
                ladrillo.GetComponent<Rigidbody>().AddForce(Vector3.right * 10f, ForceMode.VelocityChange);
                lstLadrillos.RemoveAt(0);
                Invoke("agregarMunicion", CDRegenLadrillo);
            }
        }
        else if (Input.GetKeyUp(pJugador.ataqueInput))
        {
            Invoke("resetAtaqueBasicoDistancia", 0.29f);
            Invoke("resetHaciendoAlgo", 0.29f);
        }

        if ((Time.time - timeCombo2) >= 0.5f)
        {
            ejecutandoAtaqueEspecialDer = 0;
        }
    }
    public void AtaqueEspecialIzq(Player pJugador)
    {
        //d
        if (Input.GetKeyDown(pJugador.derInput) && ejecutandoAtaqueEspecialIzq == 0)
        {
            ejecutandoAtaqueEspecialIzq = 1;

            ultTeclaCombo1 = pJugador.derInput;
            timeCombo1 = Time.time;
        }


        //a
        if (Input.GetKeyDown(pJugador.izqInput) && ejecutandoAtaqueEspecialIzq == 1 && ultTeclaCombo1 == pJugador.derInput)
        {

            ejecutandoAtaqueEspecialIzq = 2;

            ultTeclaCombo1 = pJugador.izqInput;
            timeCombo1 = Time.time;

            ejecutandoAtaqueEspecialDer = 0;
            ejecutandoAtaqueEspecialBajoDer = 0;
            ejecutandoAtaqueEspecialBajoIzq = 0;
        }


        //d
        if (Input.GetKeyDown(pJugador.derInput) && ejecutandoAtaqueEspecialIzq == 2 && ultTeclaCombo1 == pJugador.izqInput)
        {
            ejecutandoAtaqueEspecialIzq = 3;

            ultTeclaCombo1 = pJugador.derInput;
            timeCombo1 = Time.time;

            ejecutandoAtaqueEspecialDer = 0;
            ejecutandoAtaqueEspecialBajoDer = 0;
            ejecutandoAtaqueEspecialBajoIzq = 0;
        }
        //a
        if (Input.GetKeyDown(pJugador.izqInput) && ejecutandoAtaqueEspecialIzq == 3 && ultTeclaCombo1 == pJugador.derInput)
        {
            ejecutandoAtaqueEspecialIzq = 4;

            ultTeclaCombo1 = pJugador.izqInput;
            timeCombo1 = Time.time;

            ejecutandoAtaqueEspecialDer = 0;
            ejecutandoAtaqueEspecialBajoDer = 0;
            ejecutandoAtaqueEspecialBajoIzq = 0;
        }
        //e
        if (Input.GetKeyDown(pJugador.bloqueoInput) && ejecutandoAtaqueEspecialIzq == 4 && ultTeclaCombo1 == pJugador.izqInput)
        {

            ejecutandoAtaqueEspecialIzq = 0;
            ataqueBasicoDistancia = true;
            animator.SetBool("ataqueBasicoDistancia", ataqueBasicoDistancia);
            haciendoAlgo = true;

            if (lstLadrillos.Count > 0)
            {
                GameObject ladrillo = Instantiate(ladrilloBasicoPrefab, transform.position - transform.right + transform.up * 0.5f, Quaternion.identity);
                ladrillo.GetComponent<Rigidbody>().AddForce(-Vector3.right * 10f, ForceMode.VelocityChange);
                lstLadrillos.RemoveAt(0);
                Invoke("agregarMunicion", CDRegenLadrillo);

            }

        }
        else if (Input.GetKeyUp(pJugador.bloqueoInput))
        {
            Invoke("resetAtaqueBasicoDistancia", 0.29f);
            Invoke("resetHaciendoAlgo", 0.29f);
        }

        if ((Time.time - timeCombo1) >= 0.5f)
        {
            ejecutandoAtaqueEspecialIzq = 0;
        }

    }
    public void BloqueoBajo(Player pJugador)
    {

        if (Input.GetKey(pJugador.bloqueoInput) && enPiso && agachado && !ataqueBajo)
        {
            bloqueandoGolpesBajos = true;
            animator.SetBool("bloqueandoGolpeBajo", true);

        }
        else
        {
            bloqueandoGolpesBajos = false;
            animator.SetBool("bloqueandoGolpeBajo", false);
        }



    }
    public void AtaqueBajo(Player pJugador)
    {

        if (Input.GetKeyDown(pJugador.ataqueInput) && enPiso && agachado && ejecutandoAtaqueEspecialIzq < 4 && ejecutandoAtaqueEspecialDer < 4 && ejecutandoAtaqueEspecialBajoDer < 3 && ejecutandoAtaqueEspecialBajoIzq < 3)
        {
            ataqueBajo = true;
            animator.SetBool("ataqueAgachado", true);

            Invoke("resetAtaqueBajo", 0.19f);
        }

        if (!ataqueBajo)
        {
            animator.SetBool("ataqueAgachado", false);
        }

    }
    public void Agacharse(Player pJugador)
    {

        if (Input.GetKey(pJugador.abajoInput) && enPiso && !ataqueBajo)
        {
            agachado = true;
            animator.SetBool("agachado", true);
            haciendoAlgo = true;


        }

        if (Input.GetKeyUp(pJugador.abajoInput))
        {
            agachado = false;
            animator.SetBool("agachado", false);
            haciendoAlgo = false;
        }


    }
    public void AtaqueBasico(Player pJugador)
    {

        if (Input.GetKeyDown(pJugador.ataqueInput) && enPiso && !agachado && ejecutandoAtaqueEspecialIzq < 4 && ejecutandoAtaqueEspecialDer < 4 && ejecutandoAtaqueEspecialBajoDer < 3 && ejecutandoAtaqueEspecialBajoIzq < 3)
        {

            ataqueBasico = true;
            animator.SetBool("ataqueBasico", ataqueBasico);
            haciendoAlgo = true;
            Invoke("resetHaciendoAlgo", 0.25f);
            Invoke("resetAtaqueBasico", 0.19f);
        }
        else
        {

            animator.SetBool("ataqueBasico", ataqueBasico);

        }



    }

    public void ContraAtaque(Player pJugador)
    {
        if (Input.GetKeyDown(pJugador.ataqueInput) && enPiso && ejecutandoAtaqueEspecialIzq < 4 && ejecutandoAtaqueEspecialDer < 4 && ejecutandoAtaqueEspecialBajoDer < 3 && ejecutandoAtaqueEspecialBajoIzq < 3)
        {

            contraAtacando = true;
            animator.SetBool("contraAtaque", contraAtacando);
            haciendoAlgo = true;
            Invoke("resetHaciendoAlgo", 0.32f);
            Invoke("resetContraAtaque", 0.32f);
        }



    }
    public void Salto(Player pJugador)
    {

        if (Input.GetKeyDown(pJugador.arribaInput) && enPiso && !haciendoAlgo && !agachado)
        {
            vel.y += Mathf.Sqrt(fuerzaSalto * -2f * gravedad);


        }


        vel.y += gravedad * Time.deltaTime;
        characterController.Move(vel * Time.deltaTime);




    }
    public void BloqueoGolpesNormales(Player pJugador)
    {

        if (Input.GetKey(pJugador.bloqueoInput) && enPiso && !agachado && ejecutandoAtaqueEspecialIzq < 3 && ejecutandoAtaqueEspecialDer < 3 && !ataqueBasicoDistancia && !ataqueBajoDistancia)
        {
            bloqueandoGolpes = true;
            animator.SetBool("bloqueandoGolpe", bloqueandoGolpes);
            haciendoAlgo = true;
        }

        else
        {

            bloqueandoGolpes = false;
            animator.SetBool("bloqueandoGolpe", false);
            haciendoAlgo = false;

        }




    }
    public void GolpeDesdeArriba(Player pJugador)
    {

        if (Input.GetKeyDown(pJugador.ataqueInput) && saltando && !enPiso)
        {
            ataqueDesdeArriba = true;
            animator.SetBool("ataqueDesdeArriba", true);
            haciendoAlgo = true;

            Invoke("resetAtaqueDesdeArriba", 0.28f);
            Invoke("resetHaciendoAlgo", 0.28f);
        }
        if (!ataqueDesdeArriba)
        {
            animator.SetBool("ataqueDesdeArriba", false);
        }





    }

    public void resetAtaqueSuperman()
    {
        ataqueSuperman = false;
        animator.SetBool("ataqueSuperman", ataqueSuperman);
    }
    public void resetAtaqueBajoDistancia()
    {
        ataqueBajoDistancia = false;
        animator.SetBool("ataqueBajoDistancia", ataqueBajoDistancia);
    }
    public void resetAtaqueBasicoDistancia()
    {
        ataqueBasicoDistancia = false;
        animator.SetBool("ataqueBasicoDistancia", ataqueBasicoDistancia);

    }
    public void resetEjecutandoAtaqueEspecial()
    {
        ejecutandoAtaqueEspecialIzq = 0;
    }
    public void resetAtaqueDesdeArriba()
    {
        ataqueDesdeArriba = false;
    }
    public void resetAtaqueBasico()
    {
        ataqueBasico = false;
    }
    public void resetContraAtaque()
    {
        contraAtacando = false;
    }
    public void resetAtaqueBajo()
    {
        ataqueBajo = false;
    }
    public void resetHaciendoAlgo()
    {
        haciendoAlgo = false;
    }

    public void sacarVida(int da�o)
    {
        if (!conEscudo)
            vidaPlayer -= da�o;
    }


    public void resetearPos()
    {
        if (numPlayer == 1)
        {
            transform.position = GameManager.posInicialP1.position;

        }
        else if (numPlayer == 2)
        {

            transform.position = GameManager.posInicialP2.position;

        }

    }

    public void agregarMunicion()
    {
        if (lstLadrillos.Count < 3)
            lstLadrillos.Add(new GameObject());
    }

    private void desactivarEspada()
    {
        espadaDelPlayer.gameObject.SetActive(false);
    }
    private void desactivarEscudo()
    {
        escudoDelPlayer.gameObject.SetActive(false);
        conEscudo = false;
    }
    private void activarEscudo()
    {
        escudoDelPlayer.gameObject.SetActive(true);
        conEscudo = true;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Lava"))
        {
            vidaPlayer = 0;
        }
    }
    private void resetParry()
    {
        contraAtaqueHabilitado = false;
    }
    //private void resetEspadaObjeto()
    //{

    //   GameManager.resetEspadasObjetos();
    //}
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Espada"))
        {
            if (!espadaDelPlayer.isActiveAndEnabled)
            {
                sacarVida(15);
                other.GetComponentInParent<ComportamientoPersonaje1>().espadaDelPlayer.gameObject.SetActive(false);

            }


        }

        if (other.CompareTag("VidaObjeto"))
        {
            
            vidaPlayer += 7;
            other.gameObject.SetActive(false);
            GameManager.timerRecolectable = Time.time;
        }

            if (other.CompareTag("EspadaObjeto"))
        {
            other.gameObject.SetActive(false);
            GameManager.timerRecolectable = Time.time;

            espadaDelPlayer.gameObject.SetActive(true);
            Invoke("desactivarEspada", 6f);
            
        }

        if (other.CompareTag("EscudoObjeto"))
        {
            other.gameObject.SetActive(false);
            GameManager.timerRecolectable = Time.time;

            activarEscudo();
            Invoke("desactivarEscudo", 5f);
        }
        if (other.CompareTag("Lava"))
        {
            vidaPlayer = 0;
        }

        if (other.CompareTag("LadrilloBasico"))
        {
            if (bloqueandoGolpes)
            {
                recibirDa�o = false;
            }
            else if (agachado)
            {
                recibirDa�o = false;
            }
            else
            {
                recibirDa�o = true;
            }
        }
        if (other.CompareTag("LadrilloBajo"))
        {
            if (bloqueandoGolpesBajos)
            {
                recibirDa�o = false;

            }
            else
            {
                recibirDa�o = true;

            }

        }

        if (other.CompareTag("Pi�a"))
        {
            if (other.gameObject.GetComponentInParent<ComportamientoPersonaje1>().contraAtacando)
            {
                this.sacarVida(10);
                other.gameObject.GetComponentInParent<ComportamientoPersonaje1>().contraAtacando = false;
            }

            if (other.gameObject.GetComponentInParent<ComportamientoPersonaje1>().ataqueSuperman)
            {
                if (bloqueandoGolpes || bloqueandoGolpesBajos)
                    recibirDa�o = false;

                else
                {
                    this.sacarVida(6);
                    other.gameObject.GetComponentInParent<ComportamientoPersonaje1>().ataqueSuperman = false;
                }

            }

            if (other.gameObject.GetComponentInParent<ComportamientoPersonaje1>().ataqueBajo)
            {


                if (bloqueandoGolpesBajos)
                {
                    recibirDa�o = false;
                    this.sacarVida(1);
                    contraAtaqueHabilitado = true;
                    Invoke("resetParry", 0.5f);
                }
                else
                {
                    recibirDa�o = true;
                }


            }

            if (other.gameObject.GetComponentInParent<ComportamientoPersonaje1>().ataqueBasico)
            {
                if (bloqueandoGolpes)
                {
                    recibirDa�o = false;
                    this.sacarVida(1);
                    contraAtaqueHabilitado = true;
                    Invoke("resetParry", 0.5f);
                }
                else if (agachado)
                {
                    recibirDa�o = false;
                }
                else
                {
                    recibirDa�o = true;
                }
            }

            if (other.gameObject.GetComponentInParent<ComportamientoPersonaje1>().ataqueDesdeArriba)
            {
                if (bloqueandoGolpes)
                {
                    recibirDa�o = false;
                    this.sacarVida(1);
                    contraAtaqueHabilitado = true;
                    Invoke("resetParry", 0.5f);
                }
                else
                {
                    recibirDa�o = true;
                }
            }





        }

        if (recibirDa�o)
        {
            sacarVida(5);
            recibirDa�o = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Pi�a"))
        {
            animator.SetBool("stuntBasico", false);

        }
    }
}

