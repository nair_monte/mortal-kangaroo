using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorPlayer : MonoBehaviour
{
   private List<Material> matPlayer;
    private Renderer[] renderers;
 
    void Start()
    {
        matPlayer = new List<Material>();

        renderers = transform.GetChild(0).GetChild(0).GetComponentsInChildren<Renderer>();

        foreach(Renderer renderer in renderers)
        {
            matPlayer.Add(renderer.material);
        }
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void cambiarColores(string color)
    {
        foreach (Material mat in matPlayer)
        {
            switch(color)
            {
                case "rojo":
                    mat.SetColor("_Color", Color.red);
                    break;
                case "verde":
                    mat.SetColor("_Color", Color.green);
                    break;
                case "magenta":
                    mat.SetColor("_Color", Color.magenta);
                    break;
                case "azul":
                    mat.SetColor("_Color", Color.blue);
                    break;
                case "cian":
                    mat.SetColor("_Color", Color.cyan);
                    break;
                case "amarillo":
                    mat.SetColor("_Color", Color.yellow);
                    break;
            }
            
        }
    }
 
}
