using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ladrillos : MonoBehaviour
{
    private float timerVida = 5f;
    void Start()
    {
        timerVida = 5f;
    }

    // Update is called once per frame
    void Update()
    {
        timerVida -= Time.deltaTime;
        if(timerVida<=0)
        {
            Destroy(this.gameObject);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player1")|| other.gameObject.CompareTag("Player2") || other.gameObject.CompareTag("Piso"))
        {

            Destroy(this.gameObject);
        }
    }
}
