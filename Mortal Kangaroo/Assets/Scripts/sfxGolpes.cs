using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sfxGolpes : MonoBehaviour
{
    ComportamientoPersonaje1 compPersonaje;
    private int contAtaque=0;
    private int contAtaqueBajo=0;
    private int contAtaqueAlto=0;
    private int contAtaqueSuperM=0;
    void Start()
    {
        compPersonaje = GetComponent<ComportamientoPersonaje1>(); 
    }

    // Update is called once per frame
    void Update()
    {
        //ataque basico
        if (compPersonaje.ataqueBasico && contAtaque < 1)
        {
            AkSoundEngine.PostEvent("darGolpe", this.gameObject);
            contAtaque++;
        }
        else if (!compPersonaje.ataqueBasico)
            contAtaque = 0;

        //ataque bajo
        if (compPersonaje.ataqueBajo && contAtaqueBajo < 1)
        {
            AkSoundEngine.PostEvent("darGolpe", this.gameObject);
            contAtaqueBajo++;
        }
        else if (!compPersonaje.ataqueBajo)
            contAtaqueBajo = 0;

        //ataque alto
        if (compPersonaje.ataqueDesdeArriba && contAtaqueAlto < 1)
        {
            AkSoundEngine.PostEvent("darGolpe", this.gameObject);
            contAtaqueAlto++;
        }
        else if (!compPersonaje.ataqueDesdeArriba)
            contAtaqueAlto = 0;

        //ataque superman
        if (compPersonaje.ataqueSuperman && contAtaqueSuperM < 1)
        {
            AkSoundEngine.PostEvent("darGolpe", this.gameObject);
            contAtaqueSuperM++;
        }
        else if (!compPersonaje.ataqueSuperman)
            contAtaqueSuperM = 0;
    }


    IEnumerator golpesContraAtaque()
    {
        AkSoundEngine.PostEvent("darGolpe", this.gameObject);
        yield return new WaitForSeconds(0.3f);
        AkSoundEngine.PostEvent("darGolpe", this.gameObject);
    }
}
